<?php
	require('config.php');
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Seats.io API PHP Client</title>
	<meta name="author" content="Frantisek Kaspar">
	<style type="text/css">
		body {
			font-family: Monaco, "Lucida Console", monospace;
		}
		textarea {
			width: 95%;
			height: 180px;
		}
		.error {
			font-weight: bold;
			color: red;
		}
		h2 {
			background-color: #ccc;
			color: #000;
			margin: 0;
			padding: 10px;
		}
		h2.error {
			background-color: red;
			color: #000;
		}
		table {
			width: 100%;
			vertical-align: top;
		}
		td {
			vertical-align: bottom;
			width: 50%;
		}
	</style>
</head>
<body>
<h1>Seats.io API PHP client</h1>
<h3>PHP API Client for <a href="http://www.seats.io/">Seats.io</a></h2>
<ul>
<li><a href="tests/">Tests</a></li>
<li><a href="examples/">Examples</a></li>
<li><a href="docs/">Documentation</a></li>
</ul>
<p>Original API documantion: <a href="http://www.seats.io/docs">Seats.io API Docs</a></p>
<p>Source: <a href="https://github.com/seatsio/seatsio-api-php">github.com/seatsio/seatsio-api-php</a></p>
<hr>
<h3>Javascript integration of chart and event used in tests and examples.</h3>
<p>Testing booking examples you can see the seats being booked on chart here.</p>
<div id="chart"></div>
<script src="https://app.seats.io/chart.js"></script>
<script>
    new seatsio.SeatingChart({
        divId: "chart",
        publicKey: "<?=Config::$publicKey;?>",
        event: "<?=Config::$eventKey;?>"
    }).render();
</script>
</body>
</html>