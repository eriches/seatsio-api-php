<?php
/**
 * 
 * Config is purely for testing purposes
 * In you app store secretKey in your own app configuration
 * Chart, event keys are probably going to be in your database
 * 
 * */
class Config {

	/**
	 * secret key provided for account created on Seats.io
	 * */
	static $secretKey = '2b3ef28d-aa56-4400-b3ed-eb3f6ca32fbd';

	/**
	 * public key provided for account created on Seats.io
	 * */
	static $publicKey = '01a744a6-544e-4ce4-a61f-ba476c225a79';

	/**
	 * chart key is generated when chart is created on Seats.io
	 * you can get all list of all charts by charts()
	 * */
	static $chartKey = 'b53101e2-3f7c-4c7c-8ab1-0f511c0df00f';

	/**
	 * Event key is custom provided by client
	 * Testing event created on seats.io for chartkey
	 * Custom event ids can be created with linkChartToEvent() or linkChartToEvents()
	 */
	static $eventKey = 'b6e45d0e-646b-4a90-960c-361630f14e7e';

	/**
	 * identifier of general admission area - defined in seats.io chart editor
	 * there can be multiple general admission areas and ids are supplied with chart like all seats id
	 * */
	static $generalAdmissionArea = 'general-adm';

	/**
	 * custom order id used in tests
	 * */
	static $orderId = 'order20150001';
}