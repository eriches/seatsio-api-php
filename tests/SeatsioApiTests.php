<?php
/**
 * ApiTest
 * Helper class for testing cases
 * */
class ApiTest {
	public $method;
	public $request;
	public $code;
	public $type;
	public $responseData = null;
	public $error = false;
	public $errorData = null;
	public $expectedCode;
	public $expectedType;

	/**
	 * Class constructor
	 * @param  {String} $method       Test case description
	 * @param  {[type]} $expectedCode Expected http statuc code
	 * @param  {[type]} $expectedType Expected response data type
	 */
	function __construct($method,$expectedCode,$expectedType) {
		$this->method = $method;
		$this->expectedCode = $expectedCode;
		$this->expectedType = $expectedType;
	}
}

/**
 * Setup of all testing cases on API Seats.io
 * Only static functions
 * */
class SeatsioApiTests {

	/**
	 * Test request with invalid secret key
	 */
	static function invalidSecretKey() {
		$test = new ApiTest('Passing Invalid Secret Key',403,'NULL');
		$client = new SeatsioApiClient('aaabbbcccdddeeefff');
		try {
			//execute API call
			$test->responseData = $client->charts();

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Retrieve all charts for given secret key
	 */
	static function charts($data) {
		$test = new ApiTest('charts',200,'array');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->charts();

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Retrieve chart data by chart key
	 */
	static function chart($data) {
		$test = new ApiTest('chart',200,'object');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->chart($data['chartKey']);

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Retrieve chart for event
	 */
	static function eventChart($data) {
		$test = new ApiTest('eventChart',200,'object');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->eventChart($data['eventKey']);

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Book seats on chart
	 */
	static function book($data) {
		$test = new ApiTest('book',200,'NULL');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->book($data['eventKey'],$data['objects']);

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Book seats on chart. Request expected to fail, booking non-existing seats
	 */
	static function bookNotExisting($data) {
		$test = new ApiTest('bookNotExisting',200,'NULL');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->book($data['eventKey'],$data['objects']);

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Book seats on chart with order id
	 */
	static function bookOrder($data) {
		$test = new ApiTest('book - with Order ID',200,'NULL');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->book($data['eventKey'],$data['objects'],$data['orderId']);

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Release seats on chart
	 */
	static function release($data) {
		$test = new ApiTest('release',200,'NULL');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->release($data['eventKey'],$data['objects']);

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Release all seats booked on test with order id
	 */
	static function releaseOrder($data) {
		$test = new ApiTest('releaseOrder',200,'NULL');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->release($data['eventKey'],$data['objects']);

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Link event key with chart
	 * Event key can be created by client
	 */
	static function linkChartToEvent($data) {
		$test = new ApiTest('linkChartToEvent',200,'NULL');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->linkChartToEvent($data['chartKey'],$data['eventKey']);

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Link multiple event keys to one chart
	 */
	static function linkChartToEvents($data) {
		$test = new ApiTest('linkChartToEvents',200,'NULL');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->linkChartToEvents($data['chartKey'],$data['eventKeyList']);

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Create new user
	 */
	static function createUser($data) {
		$test = new ApiTest('createUser',200,'object');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->createUser();

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Create copy of chart
	 */
	static function copyChart($data) {
		$test = new ApiTest('copyChart',200,'string');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->copyChart($data['chartKey']);

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Change status of seat. Status can be custom.
	 */
	static function changeStatus($data) {
		$test = new ApiTest('changeStatus',200,'NULL');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->changeStatus($data['eventKey'],$data['status'],$data['objects']);

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Change booking status of general admission area
	 * For more info see seats.io API docs
	 */
	static function changeStatusGeneralAdmission($data) {
		$test = new ApiTest('changeStatusGeneralAdmission',200,'NULL');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->changeStatus($data['eventKey'],$data['status'],$data['objects']);

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Retrieve all objects for event and order key
	 */
	static function orders($data) {
		$test = new ApiTest('orders',200,'array');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->orders($data['eventKey'],$data['orderId']);

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}

	/**
	 * Fail test to retrieve chart with non-existing chart key
	 */
	static function chartInvalidChartKey($data) {
		$test = new ApiTest('chart - passing invalid chart key',400,'NULL');
		$client = new SeatsioApiClient($data['secretKey']);
		try {
			//execute API call
			$test->responseData = $client->chart('aaabbbcccdddeeefff');

		} catch(Exception $e) {
			$test->error = true;
			$test->errorData = $e->getMessage();
		}
		$test->request = $client->request;
		$test->code = $client->response->code;
		$test->type = gettype($test->responseData);
		return $test;
	}
}