<?php
require("../vendor/autoload.php");
require('../src/SeatsioApiClient.php');
require('SeatsioApiTests.php');
require('../config.php');

$testSet = 0;
if(!empty($_GET['set'])) $testSet = (int) $_GET['set'];

$results = array();
switch ($testSet) {
	case 1:
		$subtitle = 'Book object';
		$results[] = SeatsioApiTests::book(array('secretKey'=>Config::$secretKey,'eventKey'=>Config::$eventKey,'objects'=>array('a-2','c-7')));
		$results[] = SeatsioApiTests::bookOrder(array('secretKey'=>Config::$secretKey,'eventKey'=>Config::$eventKey, 'orderId'=> Config::$orderId,'objects'=>array('b-3','b-4','b-5')));
		$results[] = SeatsioApiTests::changeStatus(array('secretKey'=>Config::$secretKey,'eventKey'=>Config::$eventKey,'status'=>'reserved','objects'=>array('b-23','b-8','b-15')));
		$results[] = SeatsioApiTests::changeStatusGeneralAdmission(array('secretKey'=>Config::$secretKey,'eventKey'=>Config::$eventKey,'status'=>'reserved','objects'=>array(array('objectId'=>Config::$generalAdmissionArea,'quantity'=>'64'))));
		break;
	case 11:
		$subtitle = 'Release object';
		$results[] = SeatsioApiTests::release(array('secretKey'=>Config::$secretKey,'eventKey'=>Config::$eventKey,'objects'=>array('a-2','c-7')));
		$results[] = SeatsioApiTests::releaseOrder(array('secretKey'=>Config::$secretKey,'eventKey'=>Config::$eventKey,'objects'=>array('b-3','b-4','b-5')));
		$results[] = SeatsioApiTests::changeStatus(array('secretKey'=>Config::$secretKey,'eventKey'=>Config::$eventKey,'status'=>'free','objects'=>array('b-23','b-8','b-15')));
		$results[] = SeatsioApiTests::changeStatusGeneralAdmission(array('secretKey'=>Config::$secretKey,'eventKey'=>Config::$eventKey,'status'=>'free','objects'=>array(array('objectId'=>Config::$generalAdmissionArea,'quantity'=>'64'))));
		break;
	case 2:
		$subtitle = 'Create/Update chart, event, user';
		$results[] = SeatsioApiTests::linkChartToEvent(array('secretKey'=>Config::$secretKey,'chartKey'=>Config::$chartKey,'eventKey'=>'event2015001'));
		$results[] = SeatsioApiTests::linkChartToEvents(array('secretKey'=>Config::$secretKey,'chartKey'=>Config::$chartKey,'eventKeyList'=>array('event2015003', 'event2015004', 'event2015005')));
		$results[] = SeatsioApiTests::createUser(array('secretKey'=>Config::$secretKey));
		$results[] = SeatsioApiTests::copyChart(array('secretKey'=>Config::$secretKey,'chartKey'=>Config::$chartKey));
		break;
	case 3:
		$subtitle = 'Orders';
		$results[] = SeatsioApiTests::orders(array('secretKey'=>Config::$secretKey,'eventKey'=>Config::$eventKey,'orderId'=>Config::$orderId));
		break;
	case 9:
		$subtitle = 'Fail test';
		//errors - request with invalid secret key
		$results[] = SeatsioApiTests::invalidSecretKey();
		//errors - request with invalid chart key
		$results[] = SeatsioApiTests::chartInvalidChartKey(array('secretKey'=>Config::$secretKey));
		//errors - booking with non-existing seat
		$results[] = SeatsioApiTests::bookNotExisting(array('secretKey'=>Config::$secretKey,'eventKey'=>Config::$eventKey,'objects'=>array('f-2','f-7')));
		break;
	default:
		$results[] = SeatsioApiTests::charts(array('secretKey'=>Config::$secretKey));
		$results[] = SeatsioApiTests::chart(array('secretKey'=>Config::$secretKey,'chartKey'=>Config::$chartKey));
		$results[] = SeatsioApiTests::eventChart(array('secretKey'=>Config::$secretKey,'eventKey'=>Config::$eventKey));

}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?=!empty($subtitle)?$subtitle.' - ':'';?>Seats.io API PHP Client</title>
	<meta name="author" content="Frantisek Kaspar">
	<style type="text/css">
		body {
			font-family: Monaco, "Lucida Console", monospace;
		}
		textarea {
			width: 95%;
			height: 180px;
		}
		.error {
			font-weight: bold;
			color: red;
		}
		h2 {
			background-color: #ccc;
			color: #000;
			margin: 0;
			padding: 10px;
		}
		h2.error {
			background-color: red;
			color: #000;
		}
		table {
			width: 100%;
			vertical-align: top;
		}
		td {
			vertical-align: bottom;
			width: 50%;
		}
	</style>
</head>
<body>
<a href="../">[HOME]</a>
<h1>Seats.io API PHP client test cases</h1>
<h3><?=$subtitle;?></h3>
<p>Running booking cases twice will throw error with seats being already booked, run Release category test to free them up again.<br>
Orders category will give some results after Booking test are run once to create booking with order id.</p>
<ul>
<li><a href="?set=0">Basic tests</a></li>
<li><a href="?set=1">Booking tests</a></li>
<li><a href="?set=11">Release tests</a></li>
<li><a href="?set=2">Create/update event, user, chart</a></li>
<li><a href="?set=3">Orders</a></li>
<li><a href="?set=9">Error tests</a></li>
</ul>
<?php
foreach ($results as $result) {
?>
	<div>
		<h2 class="<?=$result->error || $result->code!=$result->expectedCode || $result->type!=$result->expectedType?'error':''?>"><?=$result->method;?></h2>
		<p>URL: <?=$result->request->url;?></p>
		<table><tr>
		<td>
		<h3>Request</h3>
		<textarea><?=print_r($result->request);?></textarea>
		</td>
		<td>
		<h3>Response</h3>
		<p class="<?=$result->code!=$result->expectedCode?'error':''?>">Response status: <strong><?=$result->code;?></strong> (expected: <?=$result->expectedCode;?>)</p>
		<p class="<?=$result->type!=$result->expectedType?'error':''?>">Response type: <strong><?=$result->type;?></strong> (expected: <?=$result->expectedType;?>)</p>
		<textarea><?=print_r($result->error?$result->errorData:$result->responseData);?></textarea>
		</td>
		</tr>
		</table>
		<hr>
	</div>
<?php
}
?>
</body>
</html>