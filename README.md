# Seats.io PHP API Client
A PHP library to interact with the seats.io API

## Features

* Simple function call for each feature of API
* Automatic parsing into a php object

## Requirements

- [Unirest](https://github.com/Mashape/unirest-php)
- [cURL](http://php.net/manual/en/book.curl.php)
- PHP 5.4+

## Installation

### Secret Key
Get your $secretKey from http://seats.io to be able to use API.
You will need to create account and setup your venue charts.

### Minimum installation

Composer, is used only to keep track of dependecies (Unirest library), for simplicity these are included in repo.
You can simply download vendor and src directory (files from src you can move anywhere within your project).
Include in your project: 

```
<?php
$secretKey = ' --- Your API secret key --- ';

require('vendor/autoload.php');
require('src/SeatsioApiClient.php');
$client = new SeatsioApiClient( $secretKey );

//client is ready to interact with API
//get list of all charts for account
$responseArray = $client->charts();
?>
```

### Complete source

Download the PHP library from Github:

```shell
git clone github.com/seatsio/seatsio-api-php.git
```

And you get:
- **docs/** Documentation
- **examples/** Simple examples for all API calls
- **tests/** Test cases for all API calls compared against expected status and response, including possible fail requests

## Usage

To test all examples see **examples/**

For more detailed documentation on each function, parameters, response see project documentaion in **docs/**

Or [Seats.io API docs](http://www.seats.io/docs/api)

### Booking objects
Set objects status to booked - confirmed as sold in your environment.
- objects: an array of object ids to book
- orderId(optional): an order id, defined by yourself, to be able to retrieve the objects IDs per order later on. See the Orders section below.
- reservationToken(optional): the reservation token must be supplied when booking a seat that has been temporarily reserved.
```
$client->book(['b-2','b-3'],$orderId,$reservationToken);
```

### Releasing objects
Restore status free
```
$client->release(['b-2','b-3']);
```

### Fetching a chart
Returns the full drawing: seats, categories, width, height etc.
```
$object = $client->chart($chartKey);
```

### Fetching charts of a user
This API call returns an array of chart keys, names and categories. It does not return the drawings.
```
$array = $client->charts();
```

### Fetching the chart linked to an event
This API call returns the chart key, name and categories for the chart that's linked to an event. It does not return the drawing.
```
$object = $client->eventChart($eventKey);
```

### Creating and Updating Events
For details see: [Seats.io API on Creating Event](http://www.seats.io/docs/api#creatingAndUpdatingEvents)
Id of event is create in your environment, primary id in database for example.
The first time linkChartToEvent is called, the event is created in seats.io.
```
$client->linkChartToEvent($chartKey, $eventKey);
```
or create multiple events linked to chart
```
$client->linkChartToEvents($chartKey, $arrayEventKeys);
```

### Creating users
The API lets you create new user accounts. This is useful if you own a ticketing site, and you want your clients to draw their own charts without having to register manually at seats.io.
```
$object = $client->createUser();
```

### Copying charts
Creates a copy of a chart. Events are not copied.
Response: The chartKey of the new chart
```
$newChartKey = $client->copyChart($chartKey);
```

### Changing object status
For details see: [Seats.io API: Changing status](http://www.seats.io/docs/api#changingObjectStatus)
You can use any custom status string for your app.
```
$client->changeStatus($eventKey, $status, ['a-4','a-5','b-6']);
```

### General admission
For general admission area, you also have to specify a 'quantity': the number of tickets to be booked or released.
GeneralAdmission1 is id created in chart editor on Seats.io
```
$client->changeStatus($eventKey, $status, [{'objectId': 'GeneralAdmission1', 'quantity': 3}]);
```

### Orders
When booking objects, you can optionally pass in an orderId. If you do so, you can retrieve all object IDs per orderId whenever you need (e.g. when printing tickets later on). 
Response is array of booked objects for specified event and order.
```
$client->orders($eventKey, $orderId);
```