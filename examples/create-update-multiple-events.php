<?php
require('../vendor/autoload.php');
require('../src/SeatsioApiClient.php');
require('../config.php');
//instantiate API client
$client = new SeatsioApiClient(Config::$secretKey);

//execute API request
$data = $client->linkChartToEvents(Config::$chartKey,array('event20150032','event20150033','event20150034'));

//print response data
echo '<pre>' . print_r($data,true) .'</pre>';