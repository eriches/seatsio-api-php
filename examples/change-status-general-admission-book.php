<?php
require('../vendor/autoload.php');
require('../src/SeatsioApiClient.php');
require('../config.php');
//instantiate API client
$client = new SeatsioApiClient(Config::$secretKey);

//execute API request
$data = $client->changeStatus(Config::$eventKey, 'reserved', array(array('objectId'=>Config::$generalAdmissionArea,'quantity'=>'64')));

//print response data
echo '<pre>' . print_r($data,true) .'</pre>';