<?php
require('../vendor/autoload.php');
require('../src/SeatsioApiClient.php');
require('../config.php');
//instantiate API client
$client = new SeatsioApiClient(Config::$secretKey);

//execute API request
$data = $client->book(Config::$eventKey, array('b-18','c-17'), Config::$orderId);

//print response data
echo '<pre>' . print_r($data,true) .'</pre>';