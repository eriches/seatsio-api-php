<?php
require('../vendor/autoload.php');
require('../src/SeatsioApiClient.php');
require('../config.php');
//instantiate API client
$client = new SeatsioApiClient(Config::$secretKey);

//execute API request
$data = $client->book(Config::$eventKey, array('a-2','c-7'));

//print response data
echo '<pre>' . print_r($data,true) .'</pre>';