<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Seats.io API PHP Client</title>
	<meta name="author" content="Frantisek Kaspar">
	<style type="text/css">
		body {
			font-family: Monaco, "Lucida Console", monospace;
		}
		textarea {
			width: 95%;
			height: 180px;
		}
		.error {
			font-weight: bold;
			color: red;
		}
		h2 {
			background-color: #ccc;
			color: #000;
			margin: 0;
			padding: 10px;
		}
		h2.error {
			background-color: red;
			color: #000;
		}
		table {
			width: 100%;
			vertical-align: top;
		}
		td {
			vertical-align: bottom;
			width: 50%;
		}
	</style>
</head>
<body>
<a href="../">[HOME]</a>
<h1>Seats.io API PHP client Examples</h1>
<p>Simple examples to show all API options available on Seats.io</p>

<h2>Chart loading</h2>
<ul>
<li><a href="get-charts.php">Fetching charts of a user</a></li>
<li><a href="get-chart.php">Fetching a chart</a></li>
<li><a href="get-event-chart.php">Fetching the chart linked to an event</a></li>
</ul>

<h2>Create/update event, user, chart</h2>
<ul>
<li><a href="create-update-event.php">Create/Update event</a></li>
<li><a href="create-update-multiple-events.php">Create/Update multiple events</a></li>
<li><a href="create-user.php">Create user</a></li>
<li><a href="copy-chart.php">Copy chart</a></li>
</ul>

<h2>Booking</h2>
<ul>
<li><a href="post-book.php">Book</a></li>
<li><a href="post-release.php">Release</a></li>
<li><a href="post-book-order-id.php">Booking with order ID</a></li>
<li><a href="change-status.php">Change status</a></li>
<li><a href="change-status-general-admission-book.php">Book/release/custom status general admission</a></li>
</ul>

<h2>Orders</h2>
<ul>
<li><a href="orders-for-event.php">List all orders for event</a></li>
</ul>
</body>
</html>