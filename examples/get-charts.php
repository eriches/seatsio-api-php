<?php
require('../vendor/autoload.php');
require('../src/SeatsioApiClient.php');
require('../config.php');

//instantiate API client
$client = new SeatsioApiClient(Config::$secretKey);

//execute API request
$data = $client->charts();

//print response data
echo '<pre>' . print_r($data,true) .'</pre>';